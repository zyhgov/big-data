from flask import Flask, request, jsonify
from http import HTTPStatus
from dashscope import Generation
import dashscope
app = Flask(__name__)

# 设置你的Dashscope API密钥
dashscope.api_key = 'sk-12da634744cc48a0b44f906dd0fdf3ae'

@app.route('/')
def index():
    return send_from_directory('path_to_your_static_files', 'index.html')

@app.route('/get_response', methods=['POST'])
def get_response():
    data = request.get_json()
    user_input = data.get('user_input')

    # 调用Dashscope GPT-3来获取回复
    messages = [{'role': 'user', 'content': user_input}]

    gen = Generation()
    response = gen.call(
        Generation.Models.qwen_turbo,
        messages=messages,
        result_format='message'
    )

    if response.status_code == HTTPStatus.OK:
        assistant_message = response.output.choices[0]['message']['content']
        return jsonify({"response": assistant_message})
    else:
        return jsonify({"error": "Request failed"}), HTTPStatus.INTERNAL_SERVER_ERROR

if __name__ == '__main__':
    app.run(port=5000)
