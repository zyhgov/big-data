document.addEventListener("DOMContentLoaded", function () {
    const searchInput = document.getElementById("search-input");
    const searchButton = document.getElementById("search-button");
    const resetButton = document.getElementById("reset-button");
    const fileList = document.querySelector(".file-list");
    const noResult = document.getElementById("no-result");
    let fileRows; // 声明 fileRows

    searchButton.addEventListener("click", function () {
        const searchText = searchInput.value.toLowerCase();
        fileRows = fileList.querySelectorAll(".file-row"); // 更新 fileRows

        let hasMatch = false; // 增加一个标志，表示是否有匹配的结果

        fileRows.forEach(function (row) {
            const fileName = row.querySelector(".file-name").textContent.toLowerCase();
            if (fileName.includes(searchText)) {
                row.style.display = "flex";
                hasMatch = true; // 设置标志为 true，表示有匹配结果
            } else {
                row.style.display = "none";
            }
        });

        if (!hasMatch) {
            noResult.style.display = "block";
        } else {
            noResult.style.display = "none";
        }
    });

    resetButton.addEventListener("click", function () {
        searchInput.value = "";
        if (fileRows) { // 检查 fileRows 是否定义
            fileRows.forEach(function (row) {
                row.style.display = "flex";
            });
        }
        noResult.style.display = "none";
    });
});
