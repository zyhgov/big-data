// JavaScript代码
// 获取声明框和按钮的引用
const declarationOverlay = document.getElementById("declaration-overlay");
const acknowledgeButton = document.getElementById("acknowledge-button");

// 禁用页面滚动事件
function disableScroll() {
  document.body.style.overflow = "hidden";
}

// 启用页面滚动事件
function enableScroll() {
  document.body.style.overflow = "auto";
}

// 显示声明框
window.addEventListener("load", () => {
  declarationOverlay.style.display = "flex";
  disableScroll(); // 禁用滚动
});

// 隐藏声明框并启用滚动
acknowledgeButton.addEventListener("click", () => {
  declarationOverlay.style.display = "none";
  enableScroll(); // 启用滚动
});

document.addEventListener("DOMContentLoaded", function() {
  let isImageExpanded = true;
  const imageContainer = document.getElementById('imageContainer');

  window.onscroll = function () {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      if (isImageExpanded) {
        // 收起图片
        imageContainer.style.height = '100px'; // 设置合适的收起高度
        isImageExpanded = false;
      }
    } else {
      if (!isImageExpanded) {
        // 展开图片
        imageContainer.style.height = '600px'; // 设置图片容器的高度
        isImageExpanded = true;
      }
    }
  };
});

// let index = 0;  
//         const carousel = document.getElementById('carousel');  
//         const tags = carousel.getElementsByClassName('p-tag');  
//         setInterval(function() {  
//             for (let i = 0; i < tags.length; i++) {  
//                 tags[i].classList.remove('active');  
//             }  
//             tags[index].classList.add('active');  
//             index = (index + 1) % tags.length; // 确保index不超过tags的长度  
//         }, 3000); // 2.5秒切换一次  

// window.addEventListener("load", function() {
//     // 隐藏加载页面
//     var loadingPage = document.getElementById("loading-page");
//     loadingPage.style.display = "none";
// });





// 获取音频元素
const audioElement = document.getElementById("background-music");

// 获取滚动位置来暂停/播放音乐的参考点
const playScrollPosition = document.querySelector("#loading-page").offsetTop;
const stopScrollPosition = document.querySelector("#loading-stop").offsetTop;

// 标记音乐是否已经播放
let musicPlaying = false;

// 监听页面的滚动事件
window.addEventListener("scroll", function () {
    // 获取当前滚动位置
    const currentScrollPosition = window.scrollY || window.pageYOffset;

    // 如果滚动到播放位置且音乐尚未播放，则播放音乐
    if (currentScrollPosition >= playScrollPosition && currentScrollPosition < stopScrollPosition && !musicPlaying) {
        audioElement.play();
        musicPlaying = true; // 标记音乐已经播放
    }

    // 如果滚动位置小于播放位置或大于停止位置且音乐已经播放，则暂停音乐
    if ((currentScrollPosition < playScrollPosition || currentScrollPosition >= stopScrollPosition) && musicPlaying) {
        audioElement.pause();
        musicPlaying = false; // 标记音乐已经暂停
    }
});








var button = document.querySelector('.button');
var link = document.querySelector('a');

button.addEventListener('mouseover', function() {
    this.style.backgroundColor = 'black';
    link.style.color = 'white';
});

button.addEventListener('mouseout', function() {
    this.style.backgroundColor = 'white';
    link.style.color = 'black';
});
