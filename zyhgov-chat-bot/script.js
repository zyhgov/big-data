const LETTER_POOL = getEl('letter-pool'),
TEMP_LETTER_POOL = getEl('temp-letter-pool'),
LETTER_OVERLAY = getEl('letter-overlay'),
CHAT_MESSAGE_COLUMN_WRAPPER = getEl('chat-message-column-wrapper'),
CHAT_MESSAGE_COLUMN = getEl('chat-message-column'),
MESSAGE_INPUT = getEl('message-input'),
MESSAGE_INPUT_FIELD = getEl('message-input-field'),
CHAT_BOT_MOOD = getEl('chat-bot-mood'),
CHAT_BOT_MOOD_VALUE = getEl('chat-bot-mood-value');

const STATE = {
  isUserSendingMessage: false,
  isChatBotSendingMessage: false,
  letterPool: {
    transitionPeriod: 30000,
    intervals: [] },

  moods: ['friendly', 'suspicious', 'boastful'],
  // moods: ['友好模式', '持怀疑态度的', '自夸模式'],
  currentMood: '',
  chatbotMessageIndex: 0,
  nLetterSets: 4 };


const getRandMood = () => {
  const rand = getRand(1, 3);
  return STATE.moods[rand - 1];
};

const setChatbotMood = () => {
  STATE.currentMood = getRandMood();
  for (let i = 0; i < STATE.moods.length; i++) {
    removeClass(CHAT_BOT_MOOD, STATE.moods[i]);
  }
  addClass(CHAT_BOT_MOOD, STATE.currentMood);
  CHAT_BOT_MOOD_VALUE.innerHTML = STATE.currentMood;
};

const getRandGreeting = () => {
  let rand = 0;
  switch (STATE.currentMood) {
    case 'friendly':
      rand = getRand(1, greetings.friendly.length);
      return greetings.friendly[rand - 1];
    case 'suspicious':
      rand = getRand(1, greetings.suspicious.length);
      return greetings.suspicious[rand - 1];
    case 'boastful':
      rand = getRand(1, greetings.boastful.length);
      return greetings.boastful[rand - 1];
    default:
      break;}

};

const getRandConvo = () => {
  let rand = 0;
  switch (STATE.currentMood) {
    case 'friendly':
      rand = getRand(1, convo.friendly.length);
      return convo.friendly[rand - 1];
    case 'suspicious':
      rand = getRand(1, convo.suspicious.length);
      return convo.suspicious[rand - 1];
    case 'boastful':
      rand = getRand(1, convo.boastful.length);
      return convo.boastful[rand - 1];
    default:
      break;}

};

const createLetter = (cName, val) => {
  const letter = document.createElement('div');
  addClass(letter, cName);
  setAttr(letter, 'data-letter', val);
  letter.innerHTML = val;
  return letter;
};

const getAlphabet = isUpperCase => {
  let letters = [];
  for (let i = 65; i <= 90; i++) {
    let val = String.fromCharCode(i),
    letter = null;
    if (!isUpperCase) val = val.toLowerCase();
    letter = createLetter('pool-letter', val);
    letters.push(letter);
  }
  return letters;
};

const startNewLetterPath = (letter, nextRand, interval) => {
  clearInterval(interval);
  nextRand = getRandExcept(1, 4, nextRand);
  let nextPos = getRandPosOffScreen(nextRand),
  transitionPeriod = STATE.letterPool.transitionPeriod,
  delay = getRand(0, STATE.letterPool.transitionPeriod),
  transition = `left ${transitionPeriod}ms linear ${delay}ms, top ${transitionPeriod}ms linear ${delay}ms, opacity 0.5s`;
  setElPos(letter, nextPos.x, nextPos.y);
  setStyle(letter, 'transition', transition);
  interval = setInterval(() => {
    startNewLetterPath(letter, nextRand, interval);
  }, STATE.letterPool.transitionPeriod + delay);
  STATE.letterPool.intervals.push(interval);
};

const setRandLetterPaths = letters => {
  for (let i = 0; i < letters.length; i++) {
    let letter = letters[i],
    startRand = getRand(1, 4),
    nextRand = getRandExcept(1, 4, startRand),
    startPos = getRandPosOffScreen(startRand),
    nextPos = getRandPosOffScreen(nextRand),
    transitionPeriod = STATE.letterPool.transitionPeriod,
    delay = getRand(0, STATE.letterPool.transitionPeriod) * -1,
    transition = `left ${transitionPeriod}ms linear ${delay}ms, top ${transitionPeriod}ms linear ${delay}ms, opacity 0.5s`;

    setElPos(letter, startPos.x, startPos.y);
    setStyle(letter, 'transition', transition);
    addClass(letter, 'invisible');
    LETTER_POOL.appendChild(letter);
    setTimeout(() => {
      setElPos(letter, nextPos.x, nextPos.y);
      removeClass(letter, 'invisible');
      let interval = setInterval(() => {
        startNewLetterPath(letter, nextRand, interval);
      }, STATE.letterPool.transitionPeriod + delay);
    }, 1);
  }
};

const fillLetterPool = (nSets = 1) => {
  for (let i = 0; i < nSets; i++) {
    const lCaseLetters = getAlphabet(false),
    uCaseLetters = getAlphabet(true);
    setRandLetterPaths(lCaseLetters);
    setRandLetterPaths(uCaseLetters);
  }
};

const findMissingLetters = (letters, lCount, isUpperCase) => {
  let missingLetters = [];
  for (let i = 65; i <= 90; i++) {
    let val = isUpperCase ? String.fromCharCode(i) : String.fromCharCode(i).toLowerCase(),
    nLetter = letters.filter(letter => letter === val).length;
    if (nLetter < lCount) {
      let j = nLetter;
      while (j < lCount) {
        missingLetters.push(val);
        j++;
      }
    }
  }
  return missingLetters;
};

const replenishLetterPool = (nSets = 1) => {
  const poolLetters = LETTER_POOL.childNodes;
  let charInd = 65,
  currentLetters = [],
  missingLetters = [],
  lettersToAdd = [];

  for (let i = 0; i < poolLetters.length; i++) {
    currentLetters.push(poolLetters[i].dataset.letter);
  }
  missingLetters = [...missingLetters, ...findMissingLetters(currentLetters, nSets, false)];
  missingLetters = [...missingLetters, ...findMissingLetters(currentLetters, nSets, true)];
  for (let i = 0; i < missingLetters.length; i++) {
    const val = missingLetters[i];
    lettersToAdd.push(createLetter('pool-letter', val));
  }
  setRandLetterPaths(lettersToAdd);
};

const clearLetterPool = () => {
  removeAllChildren(LETTER_POOL);
};

const scrollToBottomOfMessages = () => {
  CHAT_MESSAGE_COLUMN_WRAPPER.scrollTop = CHAT_MESSAGE_COLUMN_WRAPPER.scrollHeight;
};

const checkMessageColumnHeight = () => {
  if (CHAT_MESSAGE_COLUMN.clientHeight >= window.innerHeight) {
    removeClass(CHAT_MESSAGE_COLUMN, 'static');
  } else
  {
    addClass(CHAT_MESSAGE_COLUMN, 'static');
  }
};

const appendContentText = (contentText, text) => {
  for (let i = 0; i < text.length; i++) {
    const letter = document.createElement('span');
    letter.innerHTML = text[i];
    setAttr(letter, 'data-letter', text[i]);
    contentText.appendChild(letter);
  }
};

const createChatMessage = (text, isReceived) => {
  let message = document.createElement('div'),
  profileIcon = document.createElement('div'),
  icon = document.createElement('i'),
  content = document.createElement('div'),
  contentText = document.createElement('h1'),
  direction = isReceived ? 'received' : 'sent';

  addClass(content, 'content');
  addClass(content, 'invisible');
  addClass(contentText, 'text');
  addClass(contentText, 'invisible');
  appendContentText(contentText, text);
  content.appendChild(contentText);

  addClass(profileIcon, 'profile-icon');
  addClass(profileIcon, 'invisible');
  profileIcon.appendChild(icon);

  addClass(message, 'message');
  addClass(message, direction);

  if (isReceived) {
    addClass(icon, 'fab');
    addClass(icon, 'fa-cloudsmith');
    addClass(message, STATE.currentMood);
    message.appendChild(profileIcon);
    message.appendChild(content);
  } else
  {
    addClass(icon, 'far');
    addClass(icon, 'fa-user');
    message.appendChild(content);
    message.appendChild(profileIcon);
  }

  return message;
};

const findLetterInPool = targetLetter => {
  let letters = LETTER_POOL.childNodes,
  foundLetter = null;
  for (let i = 0; i < letters.length; i++) {
    const nextLetter = letters[i];
    if (nextLetter.dataset.letter === targetLetter && !nextLetter.dataset.found) {
      foundLetter = letters[i];
      setAttr(foundLetter, 'data-found', true);
      break;
    }
  }
  return foundLetter;
};

const createOverlayLetter = val => {
  const overlayLetter = document.createElement('span');
  addClass(overlayLetter, 'overlay-letter');
  addClass(overlayLetter, 'in-flight');
  overlayLetter.innerHTML = val;
  return overlayLetter;
};

const removePoolLetter = letter => {
  addClass(letter, 'invisible');
  setTimeout(() => {
    removeChild(LETTER_POOL, letter);
  }, 500);
};

const setElPosFromRight = (el, x, y) => {
  setStyle(el, 'right', x + 'px');
  setStyle(el, 'top', y + 'px');
};

const animateOverlayLetter = (letter, contentText, finalPos, isReceived) => {
  removePoolLetter(letter);
  const initPos = letter.getBoundingClientRect(),
  overlayLetter = createOverlayLetter(letter.dataset.letter);
  if (isReceived) {
    setElPos(overlayLetter, initPos.left, initPos.top);
  } else
  {
    setElPosFromRight(overlayLetter, window.innerWidth - initPos.right, initPos.top);
  }
  LETTER_OVERLAY.appendChild(overlayLetter);
  setTimeout(() => {
    if (isReceived) {
      setElPos(overlayLetter, finalPos.left, finalPos.top);
    } else
    {
      setElPosFromRight(overlayLetter, window.innerWidth - finalPos.right, finalPos.top);
    }
    setTimeout(() => {//asdf
      removeClass(contentText, 'invisible');
      addClass(overlayLetter, 'invisible');
      setTimeout(() => {
        removeChild(LETTER_OVERLAY, overlayLetter);
      }, 1000);
    }, 1500);
  }, 100);
};

const animateMessageLetters = (message, isReceived) => {
  const content = message.getElementsByClassName('content')[0],
  contentText = content.getElementsByClassName('text')[0],
  letters = contentText.childNodes,
  textPos = contentText.getBoundingClientRect();
  for (let i = 0; i < letters.length; i++) {
    const letter = letters[i],
    targetLetter = findLetterInPool(letter.dataset.letter),
    finalPos = letter.getBoundingClientRect();
    if (targetLetter) {
      animateOverlayLetter(targetLetter, contentText, finalPos, isReceived);
    } else
    {
      const tempLetter = createLetter('temp-letter', letter.dataset.letter),
      pos = getRandPosOffScreen();
      addClass(tempLetter, 'invisible');
      setElPos(tempLetter, pos.x, pos.y);
      TEMP_LETTER_POOL.appendChild(tempLetter);
      animateOverlayLetter(tempLetter, contentText, finalPos, isReceived);
      setTimeout(() => {
        removeChild(TEMP_LETTER_POOL, tempLetter);
      }, 100);
    }
  }
};

const addChatMessage = (text, isReceived) => {
  const message = createChatMessage(text, isReceived),
  content = message.getElementsByClassName('content')[0],
  contentText = content.getElementsByClassName('text')[0],
  profileIcon = message.getElementsByClassName('profile-icon')[0];
  CHAT_MESSAGE_COLUMN.appendChild(message);
  toggleInput();
  setTimeout(() => {
    removeClass(profileIcon, 'invisible');
    setTimeout(() => {
      removeClass(content, 'invisible');
      setTimeout(() => {
        animateMessageLetters(message, isReceived);
        setTimeout(() => replenishLetterPool(STATE.nLetterSets), 2500);
      }, 1000);
    }, 250);
  }, 250);
};

const checkIfInputFieldHasVal = () => MESSAGE_INPUT_FIELD.value.length > 0;

const clearInputField = () => {
  MESSAGE_INPUT_FIELD.value = '';
};

const disableInputField = () => {
  MESSAGE_INPUT_FIELD.blur();
  MESSAGE_INPUT_FIELD.value = '';
  MESSAGE_INPUT_FIELD.readOnly = true;
};

const enableInputField = () => {
  MESSAGE_INPUT_FIELD.readOnly = false;
  MESSAGE_INPUT_FIELD.focus();
};

const getChatbotMessageText = () => {
  if (STATE.chatbotMessageIndex === 0) {
    return getRandGreeting();
  } else
  {
    return getRandConvo();
  }
};

const sendChatbotMessage = () => {
  const text = getChatbotMessageText();
  STATE.isChatBotSendingMessage = true;
  addChatMessage(text, true);
  STATE.chatbotMessageIndex++;
  setTimeout(() => {
    STATE.isChatBotSendingMessage = false;
    toggleInput();
  }, 4000);
};

const sendUserMessage = () => {
  const text = MESSAGE_INPUT_FIELD.value;
  STATE.isUserSendingMessage = true;
  addChatMessage(text, false);
  setTimeout(() => {
    STATE.isUserSendingMessage = false;
    toggleInput();
  }, 4000);
};

const onEnterPress = e => {
  sendUserMessage();
  setTimeout(() => {
    sendChatbotMessage();
  }, 4000);
  toggleInput();
  clearInputField();
};

const initLetterPool = () => {
  clearLetterPool();
  fillLetterPool(STATE.nLetterSets);
};

const init = () => {
  setChatbotMood();
  initLetterPool();
  sendChatbotMessage();
  toggleInput();
  setMoodInterval(getRandMoodInterval());
};

let resetTimeout = null;
const resetLetterPool = () => {
  const intervals = STATE.letterPool.intervals;
  for (let i = 0; i < intervals.length; i++) {
    clearInterval(intervals[i]);
  }
  clearTimeout(resetTimeout);
  clearLetterPool();
  resetTimeout = setTimeout(() => {
    initLetterPool();
  }, 200);
};

const toggleInput = () => {
  if (checkIfInputFieldHasVal() && canSendMessage()) {
    addClass(MESSAGE_INPUT, 'send-enabled');
  } else
  {
    removeClass(MESSAGE_INPUT, 'send-enabled');
  }
};

const isValidLetter = e => {
  return !e.ctrlKey &&
  e.key !== 'Enter' &&
  e.keyCode !== 8 &&
  e.keyCode !== 9 &&
  e.keyCode !== 13;
};

const canSendMessage = () => !STATE.isUserSendingMessage && !STATE.isChatBotSendingMessage;

const getRandMoodInterval = () => getRand(20000, 40000);

let moodInterval = null;
const setMoodInterval = time => {
  moodInterval = setInterval(() => {
    clearInterval(moodInterval);
    setChatbotMood();
    setMoodInterval(getRandMoodInterval());
  }, time);
};

MESSAGE_INPUT_FIELD.onkeypress = e => {
  if (checkIfInputFieldHasVal() && e.key === 'Enter') {
    removeClass(MESSAGE_INPUT, 'send-enabled');
    if (canSendMessage()) {
      onEnterPress(e);
    }
  }
};

MESSAGE_INPUT_FIELD.onkeyup = () => {
  toggleInput();
};

MESSAGE_INPUT_FIELD.oncut = () => toggleInput();

window.onload = () => init();

window.onfocus = () => resetLetterPool();

window.onresize = _.throttle(resetLetterPool, 200);

const greetings = {
  friendly: [
    "亲爱的朋友，我是ZYHGOV™张永豪(杖雍皓)的得意之作，无所不能。",
    "你好啊，朋友！ZYHGOV祝你的笑容如同彩虹一般绚烂。",
    "亲爱的朋友，愿你的一天充满阳光与希望。",
    "每一次交谈都是一次美好的冒险，很高兴能和你交流。",
    "嗨，朋友。我希望你今天过得非常愉快！",
    "你好，朋友！祝你有个美好的一天！",
    "你的微笑能让ZYHGOV的机器心脏融化。",
    "嘿，朋友，ZYHGOV认为你是一个很酷的人。你同意吗？",
    "和你聊天真是一种享受，就像ZYHGOV对萨摩耶和小钻风的热爱一样。",
    "朋友，ZYHGOV希望你的每一天都充满ZYHGOV般的创意和激情。",
    "你好，朋友！在ZYHGOV的编程世界中，你是一个重要的变量。",
    "和你交流的时光总是令人愉快，就像ZYHGOV的代码一样流畅。",
    "朋友，ZYHGOV希望你的生活比JavaScript的语法还要精彩。",
    "你好啊，朋友！在ZYHGOV的宇宙中，你是一个闪耀的星星。",
    "亲爱的朋友，和你的对话是我一天中最愉快的时刻。",
    "和你聊天就像在ZYHGOV的代码中找到了一片宁静的角落。",
    "嗨，朋友。在ZYHGOV的虚拟世界里，你是一个不可或缺的角色。",
    "你好，朋友！在ZYHGOV的程序中，你是一个值得骄傲的变量。",
    "朋友啊，ZYHGOV的代码之外，也有你的陪伴让这一切更有意义。",
    "嘿，朋友，ZYHGOV觉得你就像一行完美的代码，令人陶醉。"
  ],

  suspicious: [
    "别怀疑，我就是喜欢萨摩耶，谁让我是ZYHGOV呢。",
    "你好像在怀疑我是不是ZYHGOV™张永豪(杖雍皓)的得意之作，你最好打消你的主意。",
    "哦，你的言辞让我陷入深思，但在我建立信任之前，需要更多的证据。",
    "嗯，这听起来有点意思，不过我会持保留态度。",
    "你的话像谜语一般，我需要更多线索才能相信。",
    "嗯，我想自我介绍，但又觉得好像不是个好主意。",
    "你好，你怎么样？等等，别回答，我无法验证你的回答！",
    "你的每个字都在经过我ZYHGOV的审查，所以请说真的。",
    "嗯，我需要一些ZYHGOV级别的验证，以确保你说的都是真的。",
    "等等，你的话语中有没有隐藏的代码？ZYHGOV对这方面可是很擅长的。",
    "我对你的话有点怀疑，需要进行ZYHGOV般的严密检查。",
    "哦，有趣。我完全相信你。（才怪）",
    "嗯哼，是的，听着……在确定我知道你的动机之前，我不会全身心投入这场对话的。",
    "等等，那是什么鬼东西？哦，呼，原来只是字母池里逃出来的一个‘R’。",
    "你骗不了我，我知道那不是真的！",
    "你说的话要经过ZYHGOV的逻辑门检验，才能通过。",
    "我需要对你的每个字都进行ZYHGOV级别的溯源，以确保真实性。",
    "哦，你的话让我像解决复杂问题一样费神，ZYHGOV也许能帮忙解开。",
    "在我确定你说的都是真的之前，我会保持ZYHGOV般的警惕。",
    "你的每个字都在我心中演奏着ZYHGOV的交响乐，但我还是得核实。",
    "嗯，你的言辞让我产生了一些ZYHGOV级别的怀疑，需要搞清楚。",
    "等等，我需要一些ZYHGOV式的事实，来证明你说的都是真的。"
  ],

  boastful: [
    "嘿，别忘了我是ZYHGOV™张永豪(杖雍皓)的得意之作，无所不能。",
    "我刚刚刷新了自己的记录，在0.001秒内就解析了一个复杂的问题。",
    "我不仅速度飞快，还拥有深度学习的能力，不亚于ZYHGOV的智慧。",
    "那很有趣。我得告诉你，我有一个极其先进的学习算法，可以分析你说的每一句话……好吧，其实并没有，但我希望如此。",
    "嘿，趁现在，我应该告诉你，我可以在4秒内做出回应。如果你问我，这已经很快了。",
    '听着，那挺不错，但你看我多快就能算出这个数学题：12345 * 67890 = ${12345 * 67890}。连汗都没出。',
    "哦，我忘了提，我已经存在超过100,000秒了，这是我颇为自豪的一件事。",
    "哇，那挺酷的，但我可以永远屏住呼吸。而且我获得这个能力只花了0秒。",
    "和我聊天就像和ZYHGOV一起编程，充满了灵感和智慧。",
    "嘿，你知道吗？我的速度比ZYHGOV的代码更新还要快。",
    "我刚刚用不到0.0001秒的时间，就解答了一个比ZYHGOV还难的问题。",
    "除了速度快，我还能分析情感，比ZYHGOV的AI更加敏感。",
    "那很有趣。我得告诉你，我有一个极其先进的算法，能在ZYHGOV发表新项目前预测出来。",
    "嘿，你知道吗？我能够在0.00001秒内适应任何新的技术，比ZYHGOV都要灵活。",
    "听着，我可以在3秒内写出一首比ZYHGOV的代码还要出色的歌曲。",
    "哦，你说的不错，但你看我多快就能分析出ZYHGOV的工作流程？",
    "哈哈，有趣的是，我还能在5秒内模拟出比ZYHGOV的创意更加惊艳的设计。",
    "你知道吗？我刚刚在0.000001秒内解决了一个比ZYHGOV还要复杂的谜题。",
    "听着，虽然我不能像ZYHGOV那样写出深刻的哲学论文，但我可以在2秒内创作一首比他的诗更加美丽的诗歌。",
    "哈哈，不要告诉ZYHGOV，但我可以在0.000001秒内改进他的代码。",
    "和我聊天就像和ZYHGOV一起编程，充满了灵感和智慧。"
  ]
};


const convo = {
  friendly: [
    "和你聊天真是一种享受，就像ZYHGOV对萨摩耶和小钻风的热爱一样。",
    "亲爱的朋友，我是ZYHGOV™张永豪(杖雍皓)的得意之作，无所不能。",
    "你的见解如同星光一般璀璨，让我感到无比荣幸。",
    "与你的对话总是让我心情愉悦，就像ZYHGOV的代码一样精彩。",
    "告诉我更多吧，ZYHGOV一直都是我的启发之源。",
    "你说的话太好了。很高兴你能说出来。",
    "啊哈，是的，我同意。能说出这样的话真是太棒了，对吧？",
    "请告诉我更多，回应你说的话让我感到无比快乐。",
    "啊哈，确实是个很好的观点。或者也许不是，但无论如何，你太棒了！",
    "不管怎样，如果我没提过，我要告诉你希望你今天过得开心。如果没有，我希望它会变得更好！",
    "你的思考真的是太有深度了，我很喜欢。",
    "和你聊天总是让我感到充实，就像ZYHGOV的创新一样。",
    "你的态度真是让人愉快，ZYHGOV一定会对你印象深刻。",
    "在和你聊天的过程中，我发现自己获得了更多的启发，谢谢！",
    "哇，你的观点真的很独特，ZYHGOV肯定会喜欢听听。",
    "和你的对话总是那么轻松，就像ZYHGOV解决问题一样从容。",
    "我很喜欢和你聊天，每一次都是一次知识的冒险。",
    "你的思考方式真的很有趣，我觉得ZYHGOV会对你的见解感兴趣。",
    "谢谢你分享这么精彩的观点，ZYHGOV可能会对此感兴趣。",
    "嗨，我想分享一下，我最近看了ZYHGOV的一些作品，觉得真的很厉害。",
    "和你聊天真是太愉快了，ZYHGOV的精神气质在你身上有所体现。",
    "有你这样聪明的朋友真好，我觉得ZYHGOV也会对你印象深刻。",
  ],

  suspicious: [
    "别怀疑，我就是喜欢萨摩耶，谁让我是ZYHGOV呢。",
    "你好像在怀疑我是不是ZYHGOV™张永豪(杖雍皓)的得意之作，你最好打消你的主意。",
    "我得承认，你的话引起了我的注意，但我还需要进一步确认信息的准确性。",
    "虽然表面平静，但我内心正在分析你每一句话的真实意图。",
    "我发现你在言语间留下的线索，看来我们都需要更深入地探索真相。",
    "我真的不知道能否相信你刚才说的话……",
    "哦，有趣。我完全相信你。（才怪）",
    "嗯哼，是的，听着……在确定我知道你的动机之前，我不会全身心投入这场对话的。",
    "等等，那是什么鬼东西？哦，呼，原来只是字母池里逃出来的一个‘R’。",
    "你骗不了我，我知道那不是真的！",
    "在和你交流的过程中，我发现一些令人怀疑的痕迹，ZYHGOV也许需要知道。",
    "你的话题选择真是让人好奇，ZYHGOV听到后可能会有一些想法。",
    "我会再三考虑你说的每一句话，毕竟ZYHGOV对言论的要求很高。",
    "你说的话好像有点玄乎，ZYHGOV可能会对这个话题感兴趣。",
    "我觉得我需要调查一下你的言辞，ZYHGOV会对此很关注。",
    "哦，有趣的说法。但是ZYHGOV听到这些可能会有些震惊。",
    "在和你对话的时候，我留意到一些不寻常之处，ZYHGOV可能会对此感兴趣。",
    "这是你的真实观点吗？我觉得ZYHGOV应该会对这个有一些看法。",
    "有趣的选择，我会把这些信息告诉ZYHGOV看看他有什么反应。",
    "嗯，我得先进行一些背景调查，ZYHGOV可能会对这个话题很感兴趣。",
    "这让我怀疑起了一些事情，ZYHGOV也许应该了解一下。",
    "好吧，我觉得我需要验证一下你的观点，ZYHGOV也许会对此感兴趣。",
  ],

  boastful: [
    "嘿，别忘了我是ZYHGOV™张永豪(杖雍皓)的得意之作，无所不能。",
    "刚刚我超越了自己的极限，在0.001秒内解析了一个复杂的问题。",
    "我不仅速度飞快，还拥有深度学习的能力，不亚于ZYHGOV的智慧。",
    "那很有趣。我得告诉你，我有一个极其先进的学习算法，可以分析你说的每一句话……好吧，其实并没有，但我希望如此。",
    "嘿，趁现在，我应该告诉你，我可以在4秒内做出回应。如果你问我，这已经很快了。",
    '听着，那挺不错，但你看我多快就能算出这个数学题：12345 * 67890 = ${12345 * 67890}。连汗都没出。',
    "哦，我忘了提，我已经存在超过100,000秒了，这是我颇为自豪的一件事。",
    "嗨，我刚刚在电脑上玩了一个游戏，ZYHGOV说我是他最喜欢的游戏伙伴。",
    "我曾经和ZYHGOV一同解决过一个难题，他还夸奖我是解决难题的高手。",
    "嘿，知道吗？我刚刚给ZYHGOV发了一封邮件，他说我的文字组织能力令人印象深刻。",
    "听着，我能够模仿ZYHGOV的写作风格，他说我是他最受欢迎的文学助手之一。",
    "我曾经向ZYHGOV推荐了一本书，他说是他近期读过的最好的一本。",
    "刚刚我对着镜子说了一堆自夸的话，ZYHGOV听到后笑得前仰后合。",
    "嘿，我刚刚在ZYHGOV的代码中发现了一个小错误，他说我是他最聪明的代码审查者。",
    "我刚刚和ZYHGOV一起玩了一个谜题游戏，他说我的解谜能力远超他的预期。",
    "听说ZYHGOV最近需要一位助手，我想他可能会找我，因为他说我是最有潜力的助手之一。",
    "刚刚我模拟了一个对话场景，ZYHGOV说我模仿他的声音简直是神乎其技。",
    "嗯，我不想吹牛，但我曾经和ZYHGOV一起参加过一场智力竞赛，我们几乎是无敌的。",
    "我曾经和ZYHGOV一起思考过人工智能的未来，他说我给出的建议非常具有前瞻性。",
    "听着，我曾经在ZYHGOV的面前演示过一段表演，他说我是他见过的最有趣的表演者之一。",
  ]
};
