  // 检查本地存储是否已经存储了访问次数，如果没有则初始化为0
  if (!localStorage.getItem('visitCount')) {
    localStorage.setItem('visitCount', 0);
}

// 获取当前访问次数
var currentVisitCount = localStorage.getItem('visitCount');

// 更新访问次数
currentVisitCount++;
localStorage.setItem('visitCount', currentVisitCount);

// 更新页面上的访问次数文本
var visitCountElement = document.getElementById('visitCount');
visitCountElement.textContent = `您使用当前设备地址打开此网站 ${currentVisitCount} 次`;




function showAlert() {
    alert("您无权限访问www.zyhgov.cn ！");
}






function checkstudentid() {
  var studentidInput = document.getElementById("studentid");
  var studentidValidation = document.getElementById("studentidValidation");

  if (/^22200\d{4}$/.test(studentidInput.value)) {
    studentidValidation.className = "validation-icon valid";
  } else {
    studentidValidation.className = "validation-icon invalid";
  }
 
}

  // 映射表：将学号映射到拼音首字母
  var studentIdToPinyin = {
  "222003854": "zy",  // 周煜的拼音首字母
  "222003855": "hq",  // 扈桥的拼音首字母
  "222003856": "gj",  // 龚俊的拼音首字母
  "222003857": "lzh", // 李子恒的拼音首字母
  "222003858": "zyn", // 朱亚男的拼音首字母
  "222003859": "la",  // 刘傲的拼音首字母
  "222003860": "ycr", // 叶长忍的拼音首字母
  "222003861": "zzy", // 张子扬的拼音首字母
  "222003862": "hsy", // 胡胜圆的拼音首字母
  "222003863": "yk",  // 叶宽的拼音首字母
  "222003864": "zzw", // 张哲炜的拼音首字母
  "222003865": "sdh", // 石东浩的拼音首字母
  "222003866": "zxp", // 张欣平的拼音首字母
  "222003867": "xxy", // 夏欣悦的拼音首字母
  "222003868": "zjy", // 张洁瑜的拼音首字母
  "222003869": "yz",  // 杨曾的拼音首字母
  "222003871": "ftk", // 方天凯的拼音首字母
  "222003872": "zyh", // 张永豪的拼音首字母
  "222003873": "czy", // 蔡紫怡的拼音首字母
  "222003875": "hs",  // 胡爽的拼音首字母
  "222003876": "flx", // 冯丽璇的拼音首字母
  "222003877": "xff", // 徐凡凡的拼音首字母
  "222003878": "ykc", // 姚凯臣的拼音首字母
  "222003879": "cmk", // 陈明康的拼音首字母
  "222003880": "wly", // 王璐瑶的拼音首字母
  "222003881": "gwj", // 顾文杰的拼音首字母
  "222003882": "ljm", // 罗江铭的拼音首字母
  "222003883": "zzy",  // 张钊俞的拼音首字母
  "222003884": "xhj", // 萧慧娟的拼音首字母
  "222003885": "qfs", // 覃凤莎的拼音首字母
  "222003886": "mxy", // 马晓艳的拼音首字母
  "222003887": "awsb", // 昂文松保的拼音首字母
  "222003888": "why", // 王鸿燕的拼音首字母
  "222003889": "mlq", // 马丽琼的拼音首字母
  "222003890": "rzdj", // 仁增多杰的拼音首字母
  "222003891": "tjw", // 田家旺的拼音首字母
  "222003892": "zq",  // 钟琴的拼音首字母
};



function checkPassword() {
  var studentidInput = document.getElementById("studentid");
  var passwordInput = document.getElementById("password");
  var passwordValidation = document.getElementById("passwordValidation");

  var studentId = studentidInput.value;
  var pinyin = studentIdToPinyin[studentId];

  if (pinyin) {
  var expectedPassword = pinyin + "123456";
  var enteredPassword = passwordInput.value;

  if (enteredPassword === expectedPassword) {
      passwordValidation.className = "validation-icon valid";
  } else {
      passwordValidation.className = "validation-icon invalid";
  }
} else {
  passwordValidation.className = "validation-icon invalid";
}
}






var welcomeMessages = {
  "222003854": "欢迎您，周煜同学，来自杖雍皓的提示！",
  "222003855": "欢迎您，扈桥同学，来自杖雍皓的提示！",
  "222003856": "欢迎您，龚俊同学，来自杖雍皓的提示！",
  "222003857": "欢迎您，李子恒同学，来自杖雍皓的提示！",
  "222003858": "欢迎您，朱亚男同学，来自杖雍皓的提示！",
  "222003859": "欢迎您，刘傲同学，来自杖雍皓的提示！",
  "222003860": "欢迎您，叶长忍同学，来自杖雍皓的提示！",
  "222003861": "欢迎您，张子扬同学，来自杖雍皓的提示！",
  "222003862": "欢迎您，胡胜圆同学，来自杖雍皓的提示！",
  "222003863": "欢迎您，叶宽同学，来自杖雍皓的提示！",
  "222003864": "欢迎您，张哲炜同学，来自杖雍皓的提示！",
  "222003865": "欢迎您，石东浩同学，来自杖雍皓的提示！",
  "222003866": "欢迎您，张欣平同学，来自杖雍皓的提示！",
  "222003867": "欢迎您，夏欣悦同学，来自杖雍皓的提示！",
  "222003868": "欢迎您，张洁瑜同学，来自杖雍皓的提示！",
  "222003869": "欢迎您，杨曾同学，来自杖雍皓的提示！",
  "222003871": "欢迎您，方天凯同学，来自杖雍皓的提示！",
  "222003872": "欢迎您，张永豪管理员，来自杖雍皓的请示！",
  "222003873": "欢迎您，蔡紫怡同学，来自杖雍皓的提示！",
  "222003875": "欢迎您，胡爽同学，来自杖雍皓的提示！",
  "222003876": "欢迎您，冯丽璇同学，来自杖雍皓的提示！",
  "222003877": "欢迎您，徐凡凡同学，来自杖雍皓的提示！",
  "222003878": "欢迎您，姚凯臣同学，来自杖雍皓的提示！",
  "222003879": "欢迎您，陈明康同学，来自杖雍皓的提示！",
  "222003880": "欢迎您，王璐瑶同学，来自杖雍皓的提示！",
  "222003881": "欢迎您，顾文杰同学，来自杖雍皓的提示！",
"222003882": "欢迎您，罗江铭同学，来自杖雍皓的提示！",
"222003883": "欢迎您，张钊俞同学，来自杖雍皓的提示！",
"222003884": "欢迎您，萧慧娟同学，来自杖雍皓的提示！",
"222003885": "欢迎您，覃凤莎同学，来自杖雍皓的提示！",
"222003886": "欢迎您，马晓艳同学，来自杖雍皓的提示！",
"222003887": "欢迎您，昂文松保同学，来自杖雍皓的提示！",
"222003888": "欢迎您，王鸿燕同学，来自杖雍皓的提示！",
"222003889": "欢迎您，马丽琼同学，来自杖雍皓的提示！",
"222003890": "欢迎您，仁增多杰同学，来自杖雍皓的提示！",
"222003891": "欢迎您，田家旺同学，来自杖雍皓的提示！",
"222003892": "欢迎您，钟琴同学，来自杖雍皓的提示！",
};

  // 222003870和222003874这两个学号的例外处理
  welcomeMessages["222003870"] = "检测到您的学号并非大数据2203班同学，请查看重新填写或退出登录";
  welcomeMessages["222003874"] = "检测到您的学号并非大数据2203班同学，请查看重新填写或退出登录";
  welcomeMessages["222003893"] = "检测到您的学号并非大数据2203班同学，请查看重新填写或退出登录";


  document.getElementById("login-form").addEventListener("submit", function(event) {
  event.preventDefault(); // 阻止表单默认提交行为

  var studentId = document.getElementById("studentid").value;
  if (studentId >= "222003854" && studentId <= "222003893" && studentId !== "222003870" && studentId !== "222003874") {
      // 学号合法，执行密码验证
      checkPassword();
  } else {
      alert("检测到您的学号并非大数据2203班同学，请查看重新填写或退出登录");
      document.getElementById("studentid").value = ""; // 清空学号输入框
      document.getElementById("password").value = ""; // 清空密码输入框
  }


    // 检查学号和密码的条件
    if (studentId >= "222003854" && studentId <= "222003893" && studentId !== "222003870" && studentId !== "222003874") {
  var enteredPassword = document.getElementById("password").value;
  
  var pinyin = studentIdToPinyin[studentId];
  
  if (pinyin) {
      var expectedPassword = pinyin + "123456";
      if (enteredPassword === expectedPassword) {
          // 密码验证通过
          var currentDate = new Date();
          var currentYear = currentDate.getFullYear();
          var currentMonth = currentDate.getMonth() + 1;
          var currentDay = currentDate.getDate();
          var currentHour = currentDate.getHours();
          var currentMinute = currentDate.getMinutes();

          var currentTimeString = `${currentYear}年${currentMonth}月${currentDay}日 ${currentHour}时${currentMinute}分`;

          var welcomeMessage = welcomeMessages[studentId] + "，" + currentTimeString;

          alert(welcomeMessage);
          window.location.href = "../homepage.html";
      } else {
          alert("Error*密码错误，请重新输入，密码要求学号对应姓名首字母+123456  错误代码: err-Initial-1 ");
          document.getElementById("password").value = ""; // 清空密码输入框
      }
  } else {
      alert("无法找到与学号匹配的拼音首字母");
  }
} else {
  alert("检测到您的学号并非大数据2203班同学，请查看重新填写或退出登录");
  document.getElementById("studentid").value = ""; // 清空学号输入框
  document.getElementById("password").value = ""; // 清空密码输入框
}

  });
  function seekHelp() {
  window.location.href = "https://yyh-0g819f773243d38b-1314221350.tcloudbaseapp.com/zixun/fuwu/zixunfuwu.htm";
}