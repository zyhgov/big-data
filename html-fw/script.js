let index = 0;  
        const carousel = document.getElementById('carousel');  
        const tags = carousel.getElementsByClassName('p-tag');  
        setInterval(function() {  
            for (let i = 0; i < tags.length; i++) {  
                tags[i].classList.remove('active');  
            }  
            tags[index].classList.add('active');  
            index = (index + 1) % tags.length; // 确保index不超过tags的长度  
        }, 3000); // 2.5秒切换一次  

window.addEventListener("load", function() {
    // 隐藏加载页面
    var loadingPage = document.getElementById("loading-page");
    loadingPage.style.display = "none";
});

// 获取标签
let nametxt=document.getElementById('name');
let button=document.getElementById('button_text');
// 创建一个数组存储名字
let uname=['-周煜 (♀)-','-扈桥 (♂)-','-龚俊 (♂)-','-李子恒 (♂)-','-朱亚男 (♀)-','-刘傲 (♂)-','-叶长忍 (♀)-','-张子扬 (♂)-','-胡胜圆 (♂)-','-叶宽 (♂)-','-张哲炜 (♂)-','-石东浩 (♂)-',
            '-张欣平 (♀)-','-夏欣悦 (♀)-','-张洁瑜 (♀)-','-杨曾 (♂)-','-方天凯 (♂)-','-张永豪 (♂)-','-蔡紫怡 (♀)-','-胡爽 (♂)-','-冯丽璇 (♀)-','-徐凡凡 (♀)-','-姚凯臣 (♂)-',
            '-陈明康 (♂)-','-王璐瑶 (♀)-','-顾文杰 (♂)-','-罗江铭 (♂)-','-张钊俞 (♂)-','-萧慧娟 (♀)-','-覃凤莎 (♀)-','-马晓艳 (♀)-','-昂文松保 (♂)-','-王鸿燕 (♀)-','-马丽琼 (♀)-','-仁增多杰 (♂)-',
            '-田家旺 (♂)-','-钟琴 (♀)-',];
// 创建一个函数生成随机数字
function getrandom(min,max){
    return Math.floor(Math.random()*(max-min-1)+min);
}
function clock(){
    // 通过获取一个随机的数组下标实现随机获取一个名字，并将这个名字赋值给变量random
    let random=uname[getrandom(0,uname.length-1)];
    //将random塞到span里
    nametxt.innerHTML=random;
};
// 打印名字已经实现了，下一步让没点击按钮前名字一直刷新
// 设置不停止时名字的刷新速度为30毫秒
let time=self.setInterval("clock()",5);
// 将开始与停止按钮绑定到按钮上，并通过按钮控制
let flag=false;
button.onclick=function(){
    // 当flag标志为false时，点击按钮让刷新停止；
    if(flag==false)
    {
        time=window.clearInterval(time);
        // 按钮文字从stop变为start；
        button.innerHTML='开始-start';
        // 标志变更
        flag=true;
    }else{
        // 当flag标志为true时，开始刷新，文字变更
        time=self.setInterval("clock()",30);
        button.innerHTML='停止-stop';
        flag=false;
    }
}